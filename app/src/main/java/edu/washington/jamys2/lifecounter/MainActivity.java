package edu.washington.jamys2.lifecounter;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    private static int playerOne;
    private static int playerTwo;
    private static int playerThree;
    private static int playerFour;
    public static final String PLAYER_ONE_SAVE = "playerOneSave";
    public static final String PLAYER_TWO_SAVE = "playerTwoSave";
    public static final String PLAYER_THREE_SAVE = "playerThreeSave";
    public static final String PLAYER_FOUR_SAVE = "playerFourSave";
    public static final int TOTAL = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        playerOneScore();
        playerTwoScore();
        playerThreeScore();
        playerFourScore();
        if (savedInstanceState != null) {
            playerOne = savedInstanceState.getInt(PLAYER_ONE_SAVE);
            playerTwo = savedInstanceState.getInt(PLAYER_TWO_SAVE);
            playerThree = savedInstanceState.getInt(PLAYER_THREE_SAVE);
            playerFour = savedInstanceState.getInt(PLAYER_FOUR_SAVE);
        } else {
            playerOne = TOTAL;
            playerTwo = TOTAL;
            playerThree = TOTAL;
            playerFour = TOTAL;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(PLAYER_ONE_SAVE, playerOne);
        savedInstanceState.putInt(PLAYER_TWO_SAVE, playerTwo);
        savedInstanceState.putInt(PLAYER_THREE_SAVE, playerThree);
        savedInstanceState.putInt(PLAYER_FOUR_SAVE, playerFour);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void press(View view) {
        switch (view.getId()) {
            case R.id.p1plus:
                playerOne++;
                playerOneScore();
                end(view);
                break;
            case R.id.p1minus:
                playerOne--;
                playerOneScore();
                end(view);
                break;
            case R.id.p1plus5:
                playerOne += 5;
                playerOneScore();
                end(view);
                break;
            case R.id.p1minus5:
                playerOne -= 5;
                playerOneScore();
                end(view);
                break;
            case R.id.p2plus:
                playerTwo++;
                playerTwoScore();
                end(view);
                break;
            case R.id.p2minus:
                playerTwo--;
                playerTwoScore();
                end(view);
                break;
            case R.id.p2plus5:
                playerTwo += 5;
                playerTwoScore();
                end(view);
                break;
            case R.id.p2minus5:
                playerTwo -= 5;
                playerTwoScore();
                end(view);
                break;
            case R.id.p3plus:
                playerThree++;
                playerThreeScore();
                end(view);
                break;
            case R.id.p3minus:
                playerThree--;
                playerThreeScore();
                end(view);
                break;
            case R.id.p3plus5:
                playerThree += 5;
                playerThreeScore();
                end(view);
                break;
            case R.id.p3minus5:
                playerThree -= 5;
                playerThreeScore();
                end(view);
                break;
            case R.id.p4plus:
                playerFour++;
                playerFourScore();
                end(view);
                break;
            case R.id.p4minus:
                playerFour--;
                playerFourScore();
                end(view);
                break;
            case R.id.p4plus5:
                playerFour += 5;
                playerFourScore();
                end(view);
                break;
            case R.id.p4minus5:
                playerFour -= 5;
                playerFourScore();
                end(view);
                break;
        }
    }

    public void playerOneScore() {
        TextView tv = (TextView)findViewById(R.id.p1total);
        tv.setText("" + playerOne);
    }

    public void playerTwoScore() {
        TextView tv = (TextView)findViewById(R.id.p2total);
        tv.setText("" + playerTwo);
    }

    public void playerThreeScore() {
        TextView tv = (TextView)findViewById(R.id.p3total);
        tv.setText("" + playerThree);
    }

    public void playerFourScore() {
        TextView tv = (TextView)findViewById(R.id.p4total);
        tv.setText("" + playerFour);
    }

    public void end(View view) {
        TextView tv = (TextView)findViewById(R.id.loser);
        if (playerOne <= 0) {
            tv.setText("Player One Loses");
        } else if (playerTwo <= 0) {
            tv.setText("Player Two Loses");
        } else if (playerThree <= 0) {
            tv.setText("Player Three Loses");
        } else if (playerFour <= 0) {
            tv.setText("Player Four Loses");
        } else {
            tv.setText("Player X Loses");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
